# AB+ MoonScript mod template

This is a template for Binding of Isaac: Afterbirth+ mods. It makes using MoonScript in your AB+ mods a lot less annoying.  
The shell script `build.sh` builds all MoonScript files inside `src/` and copies any non-MoonScript files into the mod directory.
The included makefile just calls the shell script. It's provided so that you can make use of your editor's build shortcut functionality
(like Sublime Text 3's Makefile build system).
