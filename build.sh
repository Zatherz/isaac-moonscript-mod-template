# prepare to lose your sanity trying to interpret this

# it goes through all the files in src/
# if the file ends with .moon, it will compile it using moonc
# and write it to the directory this script is ran from
# if the file does not end with .moon, it will simply be copied
# to the directory this script is ran from

# apart from that, it also writes each file that it copies or
# compiles to a file called .compiled_file_list
# when this script is ran, it will read .completed_file_list
# if it exists, and it will remove all files that are in that file
# (cleaning up the output before building again)
# it will also remove all empty directories except the src directory

# since a recent patch, 'require' works properly
# so no need to use luamerger anymore

echo >> .compiled_file_list
cat .compiled_file_list | xargs -r -L1 rm
find . -type d -not -path "./src/*" -empty -delete
echo > .compiled_file_list
find src -type f -iname "*.moon" -exec sh -c 'mkdir -p "$(dirname {})"; moonc -o "$(echo "{}" | sed -e s_src/__ -e s/\\.moon$/.lua/)" "{}"; echo "{}" | sed -e s_src/__ -e s/.moon/.lua/ >> .compiled_file_list' \;
find src -type f -not -iname "*.moon" -not -iname ".git" -exec sh -c 'mkdir -p "$(dirname "$(echo "{}" | sed -e s_src/__)")"; cp "{}" "$(dirname "$(echo "{}" | sed -e s_src/__)")/$(basename "{}")"; echo "{}" | sed -e s_src/__ -e s/.moon/.lua/ >> .compiled_file_list' \;
rm -rf .lua
