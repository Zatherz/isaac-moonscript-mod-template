# this makefile exists so that you can trigger the build
# with ctrl+b in sublime text 3
# since it has built in support for makefiles

all:
	sh build.sh
  
.PHONY: all
